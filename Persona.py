import unittest

class Persona:
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni):
        self.nombre = nombre
        self.apellidos = apellidos
        self.fecha_nacimiento = fecha_nacimiento
        self.dni = dni

    def __str__(self):
        return f"Nombre: {self.nombre}, Apellidos: {self.apellidos}, Fecha de Nacimiento: {self.fecha_nacimiento}, DNI: {self.dni}"
    
    def nombre(self):
        return self._nombre
    
    def apellidos(self):
        return self._apellidos

    def fecha_nacimiento(self):
        return self._fecha_nacimiento

    def dni(self):
        return self._dni

    def nombre(self, value):
        self._nombre = value

    def apellidos(self, value):
        self._apellidos = value

    def fecha_nacimiento(self, value):
        self._fecha_nacimiento = value

    def dni(self, value):
        self._dni = value

class Paciente(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni, historial_clinico):
        super().__init__(nombre, apellidos, fecha_nacimiento, dni)
        self.historial_clinico = historial_clinico

    def ver_historial_clinico(self):
        return f"Historial Clínico de {self.nombre}: {self.historial_clinico}"

class Médico(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni, especialidad, citas):
        super().__init__(nombre, apellidos, fecha_nacimiento, dni)
        self.especialidad = especialidad
        self.citas = citas

    def consultar_agenda(self):
        agenda = f"Citas programadas para el/la {self.especialidad} {self.nombre} {self.apellidos}:\n"
        for cita in self.citas:
            agenda = agenda + f"- {cita}\n"
        return agenda

class TestLindo(unittest.TestCase):
    def test_persona_str(self):
        persona = Persona("Alejandro", "Santos", "2/04/2088", "58375659E")
        self.assertEqual(str(persona), "Nombre: Alejandro, Apellidos: Santos, Fecha de Nacimiento: 2/04/2088, DNI: 58375659E")

    def test_historial(self):
        paciente = Paciente("Laureana", "Sánchez", "15/08/1999", "58375659X", "Lista de espera")
        self.assertEqual(paciente.ver_historial_clinico(), "Historial Clínico de Laureana: Lista de espera")

    def test_agenda(self):
        citas = ["Paciente: Primitivo Rodríguez, 25/05/1935 10:00", "Paciente: Fermín Sánchez, 25/07/1976 10:30"]
        medico = Médico("Anastasia", "Ramírez", "02/12/1971", "76986412H", "Uróloga", citas)
        agenda_esperada = "Citas programadas para el/la Uróloga Anastasia Ramírez:\n- Paciente: Primitivo Rodríguez, 25/05/1935 10:00\n- Paciente: Fermín Sánchez, 25/07/1976 10:30\n"
        self.assertEqual(medico.consultar_agenda(), agenda_esperada)

    def test_persona_setters_getters(self):
        persona = Persona("Juan Carlos", "Rivera", "28/08/1980", "48241983F")
        nuevo_nombre = "Roberto"
        persona.nombre = nuevo_nombre
        self.assertEqual(persona.nombre, nuevo_nombre)

if __name__ == '__main__':
    unittest.main()
